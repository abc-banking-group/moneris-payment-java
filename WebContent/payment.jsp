<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
                <title> Outer Frame - Merchant Page</title>

                <script>

                                function doMonerisSubmit(){
                                                var monFrameRef = document.getElementById('monerisFrame').contentWindow;
                                                monFrameRef.postMessage('tokenize','https://esqa.moneris.com/HPPtoken/index.php');
                                               //change link according to table above 
                                                return false;
                                }

                                var respMsg = function(e) {
                                                var respData = eval("(" + e.data + ")");
                                                document.getElementById("monerisResponse").innerHTML = e.origin 
                                                + " SENT " 
                                                + " response code :  " + respData.responseCode 
                                                + " datakey : " + respData.dataKey 
                                                + " BIN : " + respData.bin
                                                + " Error : " + respData.errorMessage;

                                                document.getElementById("monerisFrame").style.display = 'none';

                                                // set moneris response to hidden variable hostToken
                                                var el = document.getElementById("hostToken");
                                                el.value = respData.responseCode  + ":"+respData.dataKey +":" +respData.errorMessage;
                                             
                                }

                                window.onload = function() {
                                                if (window.addEventListener) 
                                                {  
                                                                window.addEventListener ("message", respMsg, false);
                                                }
                                                else 
                                                {
                                                                if (window.attachEvent) 
                                                                {   
                                                                window.attachEvent("onmessage", respMsg);
                                                                }
                                                }
                                }
                </script>
</head>
<body>

<div>

 		<form action="PaymentPageServlet" method="post">

                <div>MONERIS payment - HostedTokenization </div>
                <div> https://developer.moneris.com/Documentation/NA/E-Commerce%20Solutions/Hosted%20Solutions/Hosted%20Tokenization </div>
                <div> Card : 5454545454545454 <br/> Expiry(mmyy) 0420</div>
              
              
                <!--  DISPLAY MONERIS Response : Remove 'monerisResponse' division -->
                <div id=monerisResponse></div>
                 
                <!-- id : Required - Provided by the Hosted Tokenization profile configuration tool in the MRC.  -->
                <!-- css_body : Required - CSS applied to the body.  By default margin and padding is set to 0.  -->
                <!-- css_textbox : Required - CSS applied to all text boxes in general.  -->
                
                <br/>
               
                <table>
                <tr>
                <td>
	                <table>
	                <tr> <td align="right">Card Number*:</td> </tr>
	                <tr> <td align="right">Expiry Date (MMYY)*:</td> </tr>
	                <tr><td> &nbsp; </td> </tr>
	                </table>
	            </td>
                 
                <td>
                
                <iframe 
                   id=monerisFrame 
                   src="https://esqa.moneris.com/HPPtoken/index.php?id=ht14D3TG7XQCLIR&css_body=background:white;&css_textbox=border-width:2px;&css_textbox_pan=width:200px;&enable_exp=1&css_textbox_exp=width:40px;&enable_cvd=0&css_textbox_cvd=width:40px" 
                   style="border:0px"
                   frameborder="0"
                   width="200px" 
                   height="70px">
                </iframe>

				</td>
				
				</tr>
				
				<tr> 
				<td align="right"><input type=button onClick=doMonerisSubmit() value="submit payment"> </td>
				<td align="right"> <input type="submit" value="confirm payment" name ="makepayment"> </td>
				</tr>
				<!-- hidden variable to store hostedToken from MONERIS payment gateway -->
				<tr><td><input id="hostToken" type="hidden" value=""  name="hostToken" /></td></tr> 
			  	
				</table>
				
								
			</form>
					
				
	</div>
				
			
</body>
</html>