package web.moneris;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import JavaAPI.CofInfo;
import JavaAPI.HttpsPostRequest;
import JavaAPI.Receipt;
import JavaAPI.ResPurchaseCC;

/**
 * Servlet implementation class PaymentPageServlet
 */
@WebServlet("/PaymentPageServlet")
public class PaymentPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
        
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
		if(request.getParameter("makepayment")!=null) {
			
			System.out.println("Make paymnet button clicked..");
			
			String monerisResponse = request.getParameter("hostToken");
			
			if(monerisResponse!=null) {
				System.out.println("monerisResponse " + monerisResponse);
				String[] paymentToken = monerisResponse.split(":");
				String respCd = "";
				String hostedToken = "";
				String error = "";
				if(paymentToken!=null && paymentToken.length>0) {
					respCd = paymentToken[0];
					hostedToken = paymentToken[1];
					error = paymentToken[2];
					System.out.println("Response code " + respCd );
					System.out.println("HostedToken " + hostedToken);
					System.out.println("Error message " + error);
					
					if(respCd.contains("001") 
							&& paymentToken[1]!=null) {
						makePaymnet(hostedToken);
						
						PrintWriter out = response.getWriter();
						out.println("Payment success *********");
						out.println("Resp code : " + respCd);
						out.println("Hosted token : " + hostedToken);
					//	out.println("Error message  " + error);
						out.close();
						
					}else {
						// Payment failed 
						System.out.println("Payment failed ..");
						System.out.println("Response code " + respCd );
						System.out.println("HostedToken " + hostedToken);
						System.out.println("Error message " + error);
						

						PrintWriter out = response.getWriter();
						out.println("Payment failed *********");
						out.println("Resp code : " + respCd);
						out.println("Hosted token : " + hostedToken);
						out.println("Error message : " + error);
						out.close();
						
						
					}
					
				}
				
			}else {
				System.out.println("monerisResponse is null " + monerisResponse);
			}
			
			//request.getRequestDispatcher("payment.jsp").forward(request, response);
			
			
		}
		
	}

	private void makePaymnet(String hostedToken) {
		
		System.out.println("makePayment invoked ");
				
		java.util.Date createDate = new java.util.Date();
		String order_id = "Test"+createDate.getTime();
		String store_id = "store3";
		String api_token = "yesguy";
		String data_key = hostedToken;
		String amount = "1.00";
		String profile_ID = "ht14D3TG7XQCLIR";//"customer1"; //if sent will be submitted, otherwise cust_id from profile will be used
		String crypt_type = "1";
		String descriptor = "my descriptor";
		String processing_country_code = "CA";
		String expdate = "1512"; //For Temp Token
		boolean status_check = false;

		ResPurchaseCC resPurchaseCC = new ResPurchaseCC();
		resPurchaseCC.setData(data_key);
		resPurchaseCC.setOrderId(order_id);
		resPurchaseCC.setCustId(profile_ID);
		resPurchaseCC.setAmount(amount);
		resPurchaseCC.setCryptType(crypt_type);
		//resPurchaseCC.setDynamicDescriptor(descriptor);
		//resPurchaseCC.setExpDate(expdate); //Temp Tokens only

		//Mandatory - Credential on File details
		CofInfo cof = new CofInfo();
		cof.setPaymentIndicator("U");
		cof.setPaymentInformation("2");
		cof.setIssuerId("139X3130ASCXAS9");
		
		resPurchaseCC.setCofInfo(cof);
		
		HttpsPostRequest mpgReq = new HttpsPostRequest();
		mpgReq.setProcCountryCode(processing_country_code);
		mpgReq.setTestMode(true); //false or comment out this line for production transactions
		mpgReq.setStoreId(store_id);
		mpgReq.setApiToken(api_token);
		mpgReq.setTransaction(resPurchaseCC);
		mpgReq.setStatusCheck(status_check);
		mpgReq.send();

		try
		{
			Receipt receipt = mpgReq.getReceipt();

			System.out.println("DataKey = " + receipt.getDataKey());
			System.out.println("ReceiptId = " + receipt.getReceiptId());
			System.out.println("ReferenceNum = " + receipt.getReferenceNum());
			System.out.println("ResponseCode = " + receipt.getResponseCode());
			System.out.println("AuthCode = " + receipt.getAuthCode());
			System.out.println("Message = " + receipt.getMessage());
			System.out.println("TransDate = " + receipt.getTransDate());
			System.out.println("TransTime = " + receipt.getTransTime());
			System.out.println("TransType = " + receipt.getTransType());
			System.out.println("Complete = " + receipt.getComplete());
			System.out.println("TransAmount = " + receipt.getTransAmount());
			System.out.println("CardType = " + receipt.getCardType());
			System.out.println("TxnNumber = " + receipt.getTxnNumber());
			System.out.println("TimedOut = " + receipt.getTimedOut());
			System.out.println("ResSuccess = " + receipt.getResSuccess());
			System.out.println("PaymentType = " + receipt.getPaymentType());
			System.out.println("IsVisaDebit = " + receipt.getIsVisaDebit());
			System.out.println("Cust ID = " + receipt.getResCustId());
			System.out.println("Phone = " + receipt.getResPhone());
			System.out.println("Email = " + receipt.getResEmail());
			System.out.println("Note = " + receipt.getResNote());
			System.out.println("Masked Pan = " + receipt.getResMaskedPan());
			System.out.println("Exp Date = " + receipt.getResExpdate());
			System.out.println("Crypt Type = " + receipt.getResCryptType());
			System.out.println("Avs Street Number = " + receipt.getResAvsStreetNumber());
			System.out.println("Avs Street Name = " + receipt.getResAvsStreetName());
			System.out.println("Avs Zipcode = " + receipt.getResAvsZipcode());
			System.out.println("IssuerId = " + receipt.getIssuerId());
			
			
			System.out.println("CDS CORE PARAMS ");
			
			System.out.println("CardType"+ receipt.getCardType());
		   System.out.println("TransAmount"+ receipt.getTransAmount());
		   System.out.println("TransID"+ receipt.getTxnNumber());
		   System.out.println("ReceiptId"+ receipt.getReceiptId());
		   System.out.println("TransType"+ receipt.getTransType());
		   System.out.println("ReferenceNum"+ receipt.getReferenceNum());
		   System.out.println("ResponseCode"+ receipt.getResponseCode());
		   System.out.println("ISO"+ receipt.getISO());
		   System.out.println("BankTotals"+ receipt.getBankTotals());
		   System.out.println("Message"+ receipt.getMessage());
		   System.out.println("AuthCode"+ receipt.getAuthCode());
		   System.out.println("Complete"+ receipt.getComplete());
		   System.out.println("TransDate"+ receipt.getTransDate());
		   System.out.println("TransTime"+ receipt.getTransTime());
		   System.out.println("Ticket"+ receipt.getTicket());
		   System.out.println("TimedOut"+ receipt.getTimedOut());
		   System.out.println("RecurSuccess"+ "");
		   System.out.println("CvdResultCode"+ "");
		   System.out.println("AvsResultCode"+ "");
		   System.out.println("CorporateCard"+ receipt.getCorporateCard());
		   System.out.println("MessageId"+ receipt.getMessageId());
		   System.out.println("Message "+ receipt.getMessage());
		   System.out.println("Message "+ receipt.getMessage());
		   System.out.println("Token : "+receipt.getDataKey());
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		
		System.out.println("Exit of makePayment ");
		
	}

	
	
	
}
